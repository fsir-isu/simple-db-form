<?php


$subjects = [
  1 => 'Бизнес и коммуникации',
  2 => 'Технологии',
  3 => 'Реклама',
  4 => 'Маркетинг',
  5 => 'Проектирование',
  6 => 'Веб-разработка',
];


$payments = [
  1 => 'WebMoney',
  2 => 'Яндекс.Деньги',
  3 => 'PayPal',
  4 => 'Кредитная карта',
  5 => 'Робокасса',
];
