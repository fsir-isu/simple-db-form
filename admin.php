<?php

include 'variables.php';
include 'functions.php';



$pdo = get_pdo();

$sql = $pdo->prepare('SELECT * FROM `orders`;');

$sql->execute();

echo '<table border="1" style="border-collapse: collapse;">';
foreach ($sql->fetchAll() as $row)
{
  echo '<tr>';
  echo '<td>' . $row['name'] . '</td>';
  echo '<td>' . $row['lastname'] . '</td>';
  echo '<td>' . $row['tel'] . '</td>';
  echo '<td>' . $row['email'] . '</td>';
  echo '<td>' . $subjects[$row['subject']] . '</td>';
  echo '<td>' . $payments[$row['payment']] . '</td>';

  echo '</tr>';
}
echo '</table>';
