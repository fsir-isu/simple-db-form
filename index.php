<?php

include 'variables.php';
include 'functions.php';


$name      = '';
$lastname  = '';
$tel       = '';
$email     = '';
$subscribe = 1;
$subject   = 1;
$payment   = 1;

$errors = [];


if ($_POST)
{
  $name      = $_POST['name'];
  $lastname  = $_POST['lastname'];
  $tel       = $_POST['tel'];
  $email     = $_POST['email'];
  $subscribe = $_POST['subscribe'];
  $subject   = (int)$_POST['subject'];
  $payment   = (int)$_POST['payment'];


  if ($name === '')
  {
    $errors['name'] = 'Имя не может быть пустым';
  }

  if ($lastname === '')
  {
    $errors['lastname'] = 'Фамилия не может быть пустой';
  }

  if ($tel === '')
  {
    $errors['tel'] = 'Телефон нужно указать';
  }

  if ($email === '')
  {
    $errors['email'] = 'E-mail нужно указать';
  }


  if (!count($errors))
  {
    $pdo = get_pdo();

    $sql = $pdo->prepare('INSERT INTO `orders` (`name`, `lastname`, `tel`, `email`, `subject`, `payment`, `subscribe`) VALUES (:name, :lastname, :tel, :email, :subject, :payment, :subscribe);');

    $sql->execute([
      ':name' => $name,
      ':lastname' => $lastname,
      ':tel' => $tel,
      ':email' => $email,
      ':subject' => $subject,
      ':payment' => $payment,
      ':subscribe' => $subscribe,
    ]);

    $message = 'Спасибо за обращение!';
  }

}



if (isset($message))
{
  echo $message;
}
else
{

  ?>
<form action="" method="post" accept-charset="UTF-8">
  <table border="0">
    <tr>
      <td>Имя:</td>
      <td><input type="text" name="name" value="<?= $name ?>"></td>
      <td class="error"><?= array_get($errors, 'name') ?></td>
    </tr>
    <tr>
      <td>Фамилия:</td>
      <td><input type="text" name="lastname" value="<?= $lastname ?>"></td>
      <td class="error"><?= array_get($errors, 'lastname') ?></td>
    </tr>
    <tr>
      <td>Телефон:</td>
      <td><input type="text" name="tel" value="<?= $tel ?>"></td>
      <td class="error"><?= array_get($errors, 'tel') ?></td>
    </tr>
    <tr>
      <td>E-mail:</td>
      <td><input type="email" name="email" value="<?= $email ?>"></td>
      <td class="error"><?= array_get($errors, 'email') ?></td>
    </tr>
    <tr>
      <td>Подписаться на рассылку:</td>
      <td><input type="checkbox" 
        name="subscribe" 
        value="1" 
        <?= $subscribe ? ' checked' : '' ?>></td>
    </tr>
    <tr><td>Тематика:</td><td>
      <select name="subject">
        <?php foreach ($subjects as $id => $subject_name) { ?>
        <option value="<?=$id?>"<?= $subject === $id ? ' selected' : '' ?>><?=$subject_name?></option>
        <?php } ?>
      </select>
    </td></tr>
    <tr><td>Метод оплаты:</td><td>
      <select name="payment">
        <?php foreach ($payments as $id => $payment_name) { ?>
        <option value="<?=$id?>"<?= $payment === $id ? ' selected' : '' ?>><?=$payment_name?></option>
        <?php } ?>
      </select>
    </td></tr>
    <tr><td></td><td>
      <button type="submit">Отправить заявку</button>
    </td></tr>
  </table>
</form><?php

}

?>